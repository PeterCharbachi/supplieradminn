import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Suppliers } from './start-page';
import { StartPageService } from 'src/app/shared/services/start-page/start-page.service';

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.css']
})
export class StartPageComponent implements OnInit {

  suppliers: Suppliers  = {} as Suppliers

  constructor(private root: ActivatedRoute, private suppliersApi: StartPageService) { }

  ngOnInit() {
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    this.suppliersApi.getApiId(sid).subscribe(
      data => {
        this.suppliers = data;
    })
  }

}
