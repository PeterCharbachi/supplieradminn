export interface Suppliers {
    supplier_id: number;
    company_name: string;
    contact_first_name: string;
    contact_last_name: string;
    address1: string;
}