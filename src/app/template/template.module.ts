import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartPageComponent } from './start-page/start-page.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../core/auth/auth.guard';

const Routes: Routes = [
  { path: "home", component: StartPageComponent, canActivate:[AuthGuard]},
  { path: "store/:sid", component: StartPageComponent, canActivate:[AuthGuard] },
];

@NgModule({
  declarations: [
    
  ],
  imports: [
    CommonModule, 
    [RouterModule.forRoot(Routes)],
  ],
  exports: [RouterModule]
})
export class TemplateModule { }
