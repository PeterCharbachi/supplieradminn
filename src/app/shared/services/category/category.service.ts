import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { tap, map } from "rxjs/operators";
import { ICategoryView, ISubCategoryView, ISuppliersCategoryUpdate, ISuppliersCategoryView } from 'src/app/core/category/category';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http: HttpClient) { }

  host:string = 'http://localhost';
  
  deleteCategoriesById(cid): Observable < any > {
    return this.http.delete < any > (this.host + '/project/src/public/api/suppliers/categories/' + cid + '/delete').pipe(
      tap( // Log the result or error
        data => console.log(data),
      )
    );
  }
    
  deleteSubCategoriesById(sub: any): Observable < any > {
    return this.http.put < any > (this.host + '/project/src/public/api/suppliers/subcategory/delete', sub).pipe(
      tap( // Log the result or error
        data => console.log(data),
      )
    );
  }
  getCategories(): Observable<ICategoryView[]> {
    const url = this.host + '/project/src/public/api/categories';
        return this.http.get<ICategoryView[]>(url).pipe(
            tap( // Log the result or error
                data =>  console.log(data),
            )
        );
    }
    getSuppliersCategories(sid): Observable<ISuppliersCategoryView[]> {
    const url = this.host + '/project/src/public/api/supplier/categories/' + sid;
        return this.http.get<ISuppliersCategoryView[]>(url).pipe(
            tap( // Log the result or error
                data =>  console.log(data),
            )
        );
    }
    getSubCategories(sid): Observable<ISubCategoryView[]> {
      const url = this.host + '/project/src/public/api/supplier/' + sid + '/subcategories';
          return this.http.get<ISubCategoryView[]>(url).pipe(
              tap( // Log the result or error
                  data =>  console.log(data),
              )
          );
      }
    getCategoryById(sid, cid): Observable<ISuppliersCategoryUpdate> {
      const url = this.host + '/project/src/public/api/supplier/'+ sid +' /categories/'+ cid;
      return this.http.get<ISuppliersCategoryUpdate>(url).pipe(
        tap( // Log the result or error
        //   data => this.data = data
        data =>  console.log(data),
        )
      );
    }
    getSubCategoryByCategoryId(cid): Observable<ISubCategoryView[]> {
      const url = this.host + '/project/src/public/api/supplier/subcategories/' + cid;
      return this.http.get<ISubCategoryView[]>(url).pipe(
        tap( // Log the result or error
        //   data => this.data = data
        data =>  console.log(data),
        )
      );
    }

    postApiCategory(category: ICategoryView): Observable < any > {
      return this.http.post < ICategoryView > (this.host + '/project/src/public/api/supplier/categories/add', category).pipe(
        tap( // Log the result or error
          data => console.log(data),
        )
      );
    }

    postApiSubCategory(subcategory: ISubCategoryView): Observable < any > {
      return this.http.post < ISubCategoryView > (this.host + '/project/src/public/api/supplier/subcategories/add', subcategory).pipe(
        tap( // Log the result or error
          data => console.log(data),
        )
      );
    }
    putApiSubCategory(cid, subcategory: ISubCategoryView): Observable < any > {
      return this.http.put < ISubCategoryView > (this.host + '/project/src/public/api/supplier/subcategories/'+cid+'/put', subcategory).pipe(
        tap( // Log the result or error
          data => console.log(data),
        )
      );
    }
    
    putApiCategory(category: ISuppliersCategoryUpdate): Observable < any > {
      return this.http.put < ISuppliersCategoryUpdate > (this.host + '/project/src/public/api/supplier/categories/put', category).pipe(
        tap( // Log the result or error
          data => console.log(data),
        )
      );
    }
}
