import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { IUser, IUserUpdate } from 'src/app/core/users/users';





@Injectable({
    providedIn: 'root'
})
export class UserService {
    constructor(private http: HttpClient) { }

    host:string = 'http://localhost';

    getApiUsers(sid): Observable<IUser[]> {
        const url = this.host + '/project/src/public/api/supplier/'+ sid +'/user' ;
       return this.http.get<IUser[]>(url).pipe(
        tap( // Log the result or error
        //   data => this.data = data
        data =>  console.log(data),
        )
      );
    }

    
  getApiUsersById(cid): Observable<IUserUpdate> {
    const url = this.host + '/project/src/public/api/supplier/user/' + cid  ;
    return this.http.get<IUserUpdate>(url).pipe(
      tap( // Log the result or error
      //   data => this.data = data
      data =>  console.log(data),
      )
    );
  }
  deleteApiUsersById(cid): Observable < any > {
    return this.http.delete < any > (this.host + '/project/src/public/api/supplier/user/'+ cid +'/delete').pipe(
      tap( // Log the result or error
        data => console.log(data),
      )
    );
  }
  


    postApiUser(user: IUser): Observable < any > {
        return this.http.post < IUser > (this.host + '/project/src/public/api/supplier/user/add', user).pipe(
          tap( // Log the result or error
            data => console.log(data),
          )
        );
      }


    putApiUser(user: IUserUpdate): Observable < any > {
      return this.http.put < IUserUpdate > (this.host + '/project/src/public/api/supplier/user/put', user).pipe(
        tap( // Log the result or error
          data => console.log(data),
        )  
      );
    }
}