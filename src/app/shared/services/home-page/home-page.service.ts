import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { INavbar, IHeaderImage, IImageBar, IHomePageAboutUs } from 'src/app/core/homepage/homepage';





@Injectable({
    providedIn: 'root'
})
export class HomePageService {
    constructor(private http: HttpClient) { }

    host:string = 'http://localhost';

    deleteNavbarById(nav): Observable < any > {
      return this.http.delete < any > (this.host + '/project/src/public/api/supplier/homepage/navbar/'+ nav +'/delete').pipe(
        tap( // Log the result or error
          data => console.log(data),
        )
      );
    }

    deleteImagebarById(img): Observable < any > {
      return this.http.delete < any > (this.host + '/project/src/public/api/supplier/homepage_imagebar/'+ img +'/delete').pipe(
        tap( // Log the result or error
          data => console.log(data),
        )
      );
    }

    //


    getNavbarBySupplier(sid): Observable<INavbar[]> {
        const url = this.host + '/project/src/public/api/supplier/homepage/navbar/' + sid;
       return this.http.get<INavbar[]>(url).pipe(
        tap( // Log the result or error
        //   data => this.data = data
        data =>  console.log(data),
        )
      );
    }
    getNavbarBySupplierAndNavId(sid, nav): Observable<INavbar> {
      const url = this.host + '/project/src/public/api/supplier/homepage/navbar/' + sid + '/' + nav;
     return this.http.get<INavbar>(url).pipe(
      tap( // Log the result or error
      //   data => this.data = data
      data =>  console.log(data),
      )
    );
  }
    getImageHeaderBySupplier(id): Observable<IHeaderImage> {
      const url = this.host + '/project/src/public/api/supplier/homepage/imgHeader/' + id;
     return this.http.get<IHeaderImage>(url).pipe(
      tap( // Log the result or error
      //   data => this.data = data
      data =>  console.log(data),
      )
    );
  }

    getImageBarBySupplier(sid): Observable<IImageBar[]> {
      const url = this.host + '/project/src/public/api/supplier/homepage/imgBar/' + sid;
     return this.http.get<IImageBar[]>(url).pipe(
      tap( // Log the result or error
      //   data => this.data = data
      data =>  console.log(data),
      )
    );
  }

  getImageBarBySupplierAndImageId(img, sid): Observable<IImageBar> {
    const url = this.host + '/project/src/public/api/supplier/homepage/imgBar/' + img + '/' + sid;
   return this.http.get<IImageBar>(url).pipe(
    tap( // Log the result or error
    //   data => this.data = data
    data =>  console.log(data),
    )
  );
}

    postApiNavbar(navBar: INavbar): Observable < any > {
        return this.http.post < INavbar > (this.host + '/project/src/public/api/supplier/homepage/navbar/add', navBar).pipe(
          tap( // Log the result or error
            data => console.log(data),
          )
        );
      }
      putApiNavbar(navBar: INavbar): Observable < any > {
        return this.http.put < INavbar > (this.host + '/project/src/public/api/supplier/homepage/navbar/put', navBar).pipe(
          tap( // Log the result or error
            data => console.log(data),
          )
        );
      }

    postApiImageHeader(formData: any): Observable < any > {
    return this.http.post < any > (this.host + '/project/src/public/api/supplier/homepage_supplier/imageheader/add', formData).pipe(
      tap( // Log the result or error
        data => console.log(data),
      )
    );
  }
  putApiImageHeader(formData: any): Observable < any > {
    return this.http.post < any > (this.host + '/project/src/public/api/supplier/homepage_supplier/imageheader/put', formData).pipe(
      tap( // Log the result or error
        data => console.log(data),
      )
    );
  }
  postApiImagebar(formData: any): Observable < any > {
    return this.http.post < any > (this.host + '/project/src/public/api/supplier/homepage_imagebar/add', formData).pipe(
      tap( // Log the result or error
        data => console.log(data),
      )
    );
  }

  putApiImagebar(formData: any): Observable < any > {
    return this.http.post < any > (this.host + '/project/src/public/api/supplier/homepage_imagebar/put', formData).pipe(
      tap( // Log the result or error
        data => console.log(data),
      )
    );
  }

  postApiAboutUs(formData: any): Observable < any > {
    return this.http.post < any > (this.host + '/project/src/public/api/supplier/homepage_aboutus/add', formData).pipe(
      tap( // Log the result or error
        data => console.log(data),
      )
    );
  }

  putApiAboutUs(formData: any): Observable < any > {
    return this.http.put < any > (this.host + '/project/src/public/api/supplier/homepage_aboutus/put', formData).pipe(
      tap( // Log the result or error
        data => console.log(data),
      )
    );
  }

  getApiAboutUs(sid): Observable<IHomePageAboutUs> {
    const url = this.host + '/project/src/public/api/supplier/homepage_aboutus/' + sid;
   return this.http.get<IHomePageAboutUs>(url).pipe(
    tap( // Log the result or error
    //   data => this.data = data
    data =>  console.log(data),
    )
  );
}
     
}