import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { Suppliers } from 'src/app/template/start-page/start-page';




@Injectable({
    providedIn: 'root'
})
export class StartPageService {
    constructor(private http: HttpClient) { }


    host:string = 'http://localhost';
    
    data: Suppliers;

    getApiId(sid): Observable<Suppliers> {
        const url = this.host + '/project/src/public/api/suppliers/' + sid;
       return this.http.get<Suppliers>(url).pipe(
        tap( // Log the result or error
        //   data => this.data = data
        data =>  console.log(data),
        )
      );
    }

     
}