import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { IProductView } from 'src/app/core/product/products';
import { IOrderView } from 'src/app/core/order/order';


@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }

  host:string = 'http://localhost';

  getOrderBySuppliersId(sid: Number): Observable<IOrderView[]> {
    const url = this.host + '/project/src/public/api/supplier/order/' + sid ;
    return this.http.get<IOrderView[]>(url).pipe(
      tap( // Log the result or error
      //   data => this.data = data
      data =>  console.log(data),
      )
    );
  }

  getProductsByOrderId(sid, oid): Observable<IOrderView> {
    const url = this.host + '/project/src/public/api/supplier/' + sid + '/order/' + oid  ;
    return this.http.get<IOrderView>(url).pipe(
      tap( // Log the result or error
      //   data => this.data = data
      data =>  console.log(data),
      )
    );
  }

}
