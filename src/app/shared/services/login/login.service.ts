import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { tap, map } from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  host:string = 'http://localhost';


  postLogin(cred: any): Observable<any> {
    const url = this.host + '/project/src/public/api/supplier/login';
    return this.http.post<any>(url, cred).pipe(
    tap(data => console.log(JSON.stringify(data))),
    map((data: any) => {
      return data;
    }));
  }
  createLogin(signup: any): Observable<any> {
    const url = this.host + '/project/src/public/api/supplier/signup/add';
    return this.http.post<any>(url, signup).pipe(
    tap(data => console.log(JSON.stringify(data))),
    map((data: any) => {
      return data;
    }));
  }
  isLoggedIn(secret: string): Observable<any> {
    const url = this.host + '/project/src/public/api/supplier/login/' + secret;
    return this.http.get<any>(url).pipe(
    tap(data => console.log(JSON.stringify(data))),
    map((data: any) => {
      return data;
    }));
  }
}
