import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { IProductView } from 'src/app/core/product/products';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  host:string = 'http://localhost';

  deleteProductsById(pid): Observable < any > {
    return this.http.delete < any > (this.host + '/project/src/public/api/suppliers/products/' + pid + '/delete').pipe(
      tap( // Log the result or error
        data => console.log(data),
      )
    );
  }

  getProductsBySuppliersId(sid: Number): Observable<IProductView[]> {
    const url = this.host + '/project/src/public/api/suppliers/' + sid + '/products' ;
    return this.http.get<IProductView[]>(url).pipe(
      tap( // Log the result or error
      //   data => this.data = data
      data =>  console.log(data),
      )
    );
  }

  getProductsById(sid, pid): Observable<IProductView> {
    const url = this.host + '/project/src/public/api/suppliers/' + sid + '/products/' + pid  ;
    return this.http.get<IProductView>(url).pipe(
      tap( // Log the result or error
      //   data => this.data = data
      data =>  console.log(data),
      )
    );
  }

  postApiProducts(formData: any): Observable < any > {
    return this.http.post < any > (this.host + '/project/src/public/api/suppliers/products/add', formData).pipe(
      tap( // Log the result or error
        data => console.log(data),
      )
    );
  }

  putApiProducts(formData: any): Observable < any > {
    return this.http.post < any > (this.host + '/project/src/public/api/supplier/products/put', formData).pipe(
      tap( // Log the result or error
        data => console.log(data),
      )
    );
  }

}
