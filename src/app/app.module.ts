import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule, MatPaginatorModule, MatSortModule, MatFormFieldModule, MatInputModule, MatRippleModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StartPageComponent } from './template/start-page/start-page.component';
import { LoginViewComponent } from './core/login/login-view/login-view.component';
import { LoginCreateComponent } from './core/login/login-create/login-create.component';
import { CategoryViewComponent } from './core/category/category-view/category-view.component';
import { CategoryDetailComponent } from './core/category/category-detail/category-detail.component';
import { CategoryCreateComponent } from './core/category/category-create/category-create.component';
import { HomePageViewComponent } from './core/homepage/home-page-view/home-page-view.component';
import { AuthGuard } from './core/auth/auth.guard';
import { UsersViewComponent } from './core/users/users-view/users-view.component';
import { UsersDetailComponent } from './core/users/users-detail/users-detail.component';
import { UsersCreateComponent } from './core/users/users-create/users-create.component';


@NgModule({
  declarations: [
    AppComponent,
    StartPageComponent,
    LoginViewComponent,
    LoginCreateComponent,
    CategoryViewComponent,
    CategoryDetailComponent,
    CategoryCreateComponent,
    HomePageViewComponent,
    UsersViewComponent,
    UsersDetailComponent,
    UsersCreateComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule, 
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    ReactiveFormsModule,
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
