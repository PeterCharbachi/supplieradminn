import { Component } from '@angular/core';
import { LoginService } from './shared/services/login/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'suppliers-admin';
  secret: boolean = false;


  constructor(private loginApi: LoginService) {}


  ngOnInit() {
    // let sid = JSON.parse(sessionStorage.getItem('secret'));
    // sid = sid.secret
      
    // this.loginApi.isLoggedIn(sid).subscribe(
    //   data => {
    //     return true;
    // })
    
  }
  isLoggedIn() {
    let data = JSON.parse(sessionStorage.getItem('secret'));
    if(data === null) 
      return null;
    return data.secret = true;
  }
}