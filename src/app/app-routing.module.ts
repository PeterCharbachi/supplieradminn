import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TemplateModule } from './template/template.module';
import { ProductsModule } from './core/product/products.module';
import { StartPageComponent } from './template/start-page/start-page.component';
import { LoginModule } from './core/login/login.module';
import { CategoryModule } from './core/category/category.module';
import { HomepageModule } from './core/homepage/homepage.module';
import { AuthGuard } from './core/auth/auth.guard';
import { OrderModule } from './core/order/order.module';
import { UsersModule } from './core/users/users.module';


const routes: Routes = [
  { path: '', component: StartPageComponent, canActivate:[AuthGuard] },
  { path: '**', redirectTo: '/home', pathMatch: 'full', canActivate:[AuthGuard] },
];

@NgModule({
  imports: [
    TemplateModule,
    ProductsModule,
    LoginModule,
    CategoryModule,
    HomepageModule,
    OrderModule,
    UsersModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
