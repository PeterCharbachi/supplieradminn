import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from 'src/app/shared/services/products/products.service';
import { IProductUpdate } from '../products';
import { CategoryService } from 'src/app/shared/services/category/category.service';
import { ISuppliersCategoryView, ICategoryView } from '../../category/category';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  
  products: IProductUpdate  = {} as IProductUpdate
  selectedFile: File;
  suppliersCategories: ISuppliersCategoryView[] = [];
  categories: ICategoryView[] = [];
  selectedSuppliersCategory:string = '0';
  selectedCategory:string = '0';

  constructor(private root: ActivatedRoute, private suppliersApi: ProductsService, private _router: Router, private category: CategoryService) { }



  ngOnInit() {
    this.products.picture = [];


    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    const pid = this.root.snapshot.paramMap.get('pid');
    this.suppliersApi.getProductsById(sid, pid).subscribe(
      data => {
        this.products = data;
        let categoryId:string =  this.products.category_id.toString();
        let supplier_category_id:string =  this.products.supplier_category_id.toString();
        console.log(categoryId);
        this.selectedCategory = categoryId;
        this.selectedSuppliersCategory = supplier_category_id;

    })

    this.category.getSuppliersCategories(sid).subscribe(
      data => {
        this.suppliersCategories = data;
    })

    this.category.getCategories().subscribe(
      data => {
        this.categories = data;
    })


  }


  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
 }


 onChange(){
  // const formData = new FormData();
  // formData.append('image', this.selectedFile, this.selectedFile.name);
  // console.log(formData);
  // var options = { content: formData };
  // let cid:number =  parseInt(this.selectedCategory);
  // this.products.category_id = cid
  
  let sid = JSON.parse(sessionStorage.getItem('secret'));
  sid = sid.user
  this.products.supplier_id = sid;
  const productId:any = this.products.product_id;
  const price:any = this.products.price;
  const product_name:any = this.products.product_name;
  const category_id:any = this.products.category_id;
  const sku:any = this.products.sku;
  const product_description:any = this.products.product_description;

  let formData = new FormData();
  formData.append('file', this.selectedFile);
  formData.append('product_name', product_name);
  formData.append('category_id', this.selectedCategory);
  formData.append('supplier_category_id', this.selectedSuppliersCategory)
  formData.append('price', price);
  formData.append('sku', sku);
  formData.append('product_description', product_description);
  formData.append('product_id', productId);



  // this.http.post('http://localhost/project/src/public/api/suppliers/products/add', formData).subscribe(data => console.log(data));
  this.suppliersApi.putApiProducts(formData).subscribe(
    (data => {
      console.log(data);
      this._router.navigateByUrl('/products');
    })
  )
}

onDelete(){
  const pid = this.root.snapshot.paramMap.get('pid');

  this.suppliersApi.deleteProductsById(pid).subscribe(
    (data => {
      console.log(data);
      this._router.navigateByUrl('products');
    })
  )

}

}
