export interface IProductUpdate {
    product_id: number;
    sku: string;
    product_name: string;
    product_description: string;
    supplier_id: number;
    category_id: number;
    price: number;
    supplier_category_id:number
    
    picture: PictureInfo[];
    

}

export interface IProductView {
    product_id: number;
    sku: string;
    product_name: string;
    product_description: string;
    supplier_id: number;
    category_id: number;
    price: number;
    supplier_category_id:number
    
    picture: PictureInfo[];
    

}

export interface PictureInfo {
    name: string;
    size: number;
    type: string;
}
