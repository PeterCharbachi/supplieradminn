import { Component, OnInit, NgModule } from '@angular/core';
import { IProductView, PictureInfo } from '../products';
import { Router } from '@angular/router';
import { ProductsService } from 'src/app/shared/services/products/products.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CategoryService } from 'src/app/shared/services/category/category.service';
import { ICategoryView, ISuppliersCategoryView } from '../../category/category';
@Component({
  selector: 'app-product-create',
  templateUrl: './product-create.component.html',
  styleUrls: ['./product-create.component.css']
})
export class ProductCreateComponent implements OnInit {
  products: IProductView = {} as IProductView;
  pictureInfo: PictureInfo = {} as PictureInfo
  selectedFile: File;
  suppliersCategories: ISuppliersCategoryView[] = [];
  categories: ICategoryView[] = [];
  selectedSuppliersCategory:string = '0';
  selectedCategory:string = '0';

  constructor(private suppliersApi: ProductsService, private _router: Router, private http: HttpClient, private category: CategoryService) { }

  ngOnInit() {
    this.products.picture = [];

    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    this.category.getSuppliersCategories(sid).subscribe(
      data => {
        this.suppliersCategories = data;
    })

    this.category.getCategories().subscribe(
      data => {
        this.categories = data;
    })
  }


  onFileChanged(event) {
      this.selectedFile = event.target.files[0];
   }

  //  upload() {
  //   this.suppliersApi.postApiProducts(this.products, this.selectedFile).subscribe(
  //     .subscribe(data => {
  //           gör vad du vill här 🙂
  // });
  // }
  onSave(){
    // const formData = new FormData();
    // formData.append('image', this.selectedFile, this.selectedFile.name);
    // console.log(formData);
    // var options = { content: formData };
    let Scid:number =  parseInt(this.selectedSuppliersCategory);
    this.products.supplier_category_id = Scid
    
    let cid:number =  parseInt(this.selectedCategory);
    this.products.category_id = cid


    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    this.products.supplier_id = sid;
    const price:any = this.products.price;
    const product_name:any = this.products.product_name;
    const supplier_category_id:any = this.products.supplier_category_id;
    const category_id:any = this.products.category_id;
    const sku:any = this.products.sku;
    const product_description:any = this.products.product_description;

    let formData = new FormData();
    formData.append('file', this.selectedFile);
    formData.append('supplier_id', this.products.supplier_id = sid);
    formData.append('product_name', product_name);
    formData.append('supplier_category_id', supplier_category_id);
    formData.append('category_id', category_id);
    formData.append('price', price);
    formData.append('sku', sku);
    formData.append('product_description', product_description);



    // this.http.post('http://localhost/project/src/public/api/suppliers/products/add', formData).subscribe(data => console.log(data));
    this.suppliersApi.postApiProducts(formData).subscribe(
      (data => {
        console.log(data);
        this._router.navigateByUrl('/products');
      })
    )
  }
}
