import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductViewComponent } from './product-view/product-view.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductCreateComponent } from './product-create/product-create.component';
import { Routes, RouterModule } from '@angular/router';
import { MatInputModule, MatFormFieldModule, MatTableModule, MatPaginatorModule, MatSortModule, MatRippleModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuard } from '../auth/auth.guard';

const Routes: Routes = [
  { path: "products", component: ProductViewComponent, canActivate:[AuthGuard] },
  { path: "products/create", component: ProductCreateComponent, canActivate:[AuthGuard] },
  { path: "product/:pid/edit", component: ProductDetailComponent, canActivate:[AuthGuard] },
];

@NgModule({
  declarations: [
    ProductViewComponent, 
    ProductDetailComponent, 
    ProductCreateComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    ReactiveFormsModule, 
    
    [RouterModule.forRoot(Routes)],
  ],
  exports: [RouterModule]
})
export class ProductsModule { }
