import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductsService } from 'src/app/shared/services/products/products.service';
import { IProductView } from '../products';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.css']
})
export class ProductViewComponent implements OnInit {

  products: IProductView[];
  displayedColumns = ['id', 'name', 'description', 'category_id', 'price'];
  dataSource: MatTableDataSource<IProductView>;
 
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private suppliersApi: ProductsService) {
     this.dataSource = new MatTableDataSource();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
  
    this.suppliersApi.getProductsBySuppliersId(sid).subscribe(
      data => {
        this.dataSource.data = data;
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
