import { Component, OnInit } from '@angular/core';
import { IUser } from '../users';
import { UserService } from 'src/app/shared/services/user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users-create',
  templateUrl: './users-create.component.html',
  styleUrls: ['./users-create.component.css']
})
export class UsersCreateComponent implements OnInit {
  user: IUser = {} as IUser


  constructor(private suppliersApi: UserService,  private _router: Router) { }

  ngOnInit() {
  }


  saveUser(){
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    this.user.supplierId = sid;
    this.suppliersApi.postApiUser(this.user).subscribe(
      (data => { 
        this._router.navigateByUrl('/users');
      })
    )
    
  }
}
