import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AuthGuard } from '../auth/auth.guard';
import { UsersViewComponent } from './users-view/users-view.component';
import { UsersDetailComponent } from './users-detail/users-detail.component';
import { UsersCreateComponent } from './users-create/users-create.component';



const Routes: Routes = [
  { path: "users", component: UsersViewComponent, canActivate:[AuthGuard]},
  { path: "users/:cid/edit", component: UsersDetailComponent, canActivate:[AuthGuard] },
  { path: "users/create", component: UsersCreateComponent, canActivate:[AuthGuard] },
  // { path: "homepage/image-bar", component: HomePageImageBarComponent, canActivate:[AuthGuard] },
  // { path: "homepage/preview", component: HomePagePreviewComponent, canActivate:[AuthGuard] },
  // { path: "homepage/about-us", component: HomePageAboutUsComponent, canActivate:[AuthGuard] },

];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule,
    [RouterModule.forRoot(Routes)],
  ]
})
export class UsersModule { }
