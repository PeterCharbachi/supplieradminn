import { Component, OnInit } from '@angular/core';
import { IUserUpdate } from '../users';
import { UserService } from 'src/app/shared/services/user/user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-users-detail',
  templateUrl: './users-detail.component.html',
  styleUrls: ['./users-detail.component.css']
})
export class UsersDetailComponent implements OnInit {
  user: IUserUpdate = {} as IUserUpdate;
  constructor(private root: ActivatedRoute, private suppliersApi: UserService, private _router: Router,) { }

  ngOnInit() {

    
    // let sid = JSON.parse(sessionStorage.getItem('secret'));
    // sid = sid.user
    const cid = this.root.snapshot.paramMap.get('cid');
    this.suppliersApi.getApiUsersById(cid).subscribe(
      data => {
        this.user = data;
    })
  }



  updateUser() {

    this.suppliersApi.putApiUser(this.user).subscribe(
      data => {
        this.user = data;
        this._router.navigateByUrl('/users');
      }
    )
  }

  onDelete(){
    const cid = this.root.snapshot.paramMap.get('cid');
  
    this.suppliersApi.deleteApiUsersById(cid).subscribe(
      (data => {
        console.log(data);
        this._router.navigateByUrl('/users');
      })
    )
  
  }

}
