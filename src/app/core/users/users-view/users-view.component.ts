import { Component, OnInit, ViewChild } from '@angular/core';
import { IUser } from '../users';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { UserService } from 'src/app/shared/services/user/user.service';


@Component({
  selector: 'app-users-view',
  templateUrl: './users-view.component.html',
  styleUrls: ['./users-view.component.css']
})
export class UsersViewComponent implements OnInit {
  users: IUser[];
  displayedColumns = ['id', 'first_name', 'last_name', 'address1', 'address2', 'phone', 'email'];
  dataSource: MatTableDataSource<IUser>;
 
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private suppliersApi: UserService) {
     this.dataSource = new MatTableDataSource();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
  
    this.suppliersApi.getApiUsers(sid).subscribe(
      data => {
        this.dataSource.data = data;
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
