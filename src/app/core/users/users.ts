export interface IUserUpdate {
    supplierId: number
    firstName: string;
    lastName: string;
    address1: string;
    address2: string;
    phone: number;
    email: string;
    password: string;
    customer_id: number;
}


export interface IUser {
    supplierId: number
    firstName: string;
    lastName: string;
    address1: string;
    address2: string;
    phone: number;
    email: string;
    password: string;
    customer_id: number;
}