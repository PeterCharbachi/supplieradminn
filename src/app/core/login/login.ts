export interface Ilogin{
    username: string;
    password: string;
}

export interface ISignUp {
    companyName: string;
    contactFirstName: string;
    contactLastName: string;
    supplierAddress1: string;
    supplierAddress2: string;
    supplierPhone: number;
    supplierEmail: string;

    firstName: string;
    lastName: string;
    address1: string;
    address2: string;
    phone: number;
    email: string;
    password: string;

}