import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LoginViewComponent } from './login-view/login-view.component';
import { LoginCreateComponent } from './login-create/login-create.component';

const Routes: Routes = [
  { path: "login", component: LoginViewComponent },
  { path: "login/create", component: LoginCreateComponent },
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    [RouterModule.forRoot(Routes)],
  ],
  exports: [RouterModule]
})
export class LoginModule { }
