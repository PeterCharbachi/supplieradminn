import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/shared/services/login/login.service';
import { Router } from '@angular/router';
import { Ilogin, ISignUp } from '../login';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.css']
})
export class LoginViewComponent implements OnInit {
  tabIndex: number = 1;
  cred: Ilogin = {} as Ilogin;
  signUp: ISignUp = {} as ISignUp;

  messageForm: FormGroup;
  submitted = false;
  success: boolean;
  username: string;
  password: string;
  constructor(private loginApi: LoginService, private _router: Router, private formBuilder: FormBuilder) { 
    this.messageForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  ngOnInit() {
  }
  onSignUp() {

    this.loginApi.createLogin(this.signUp).subscribe(
      (data => {
        this.tabIndex = 1;
      })
    )

  }

  login(){
    this.username = this.cred.username;
    this.password = this.cred.password;

    this.submitted = true;
    if (this.messageForm.invalid) {
      return this.success = false;
    }

    if (this.messageForm.valid) {
      
      this.loginApi.postLogin(this.cred).subscribe(
        data => {
          sessionStorage.setItem('secret', JSON.stringify(data));
          if(data.secret != null) {
            this._router.navigateByUrl('/home');
          }
      })
      return this.success = true;
    }

  }
}
