import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomePageViewComponent } from './home-page-view/home-page-view.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HomePagePreviewComponent } from './home-page-preview/home-page-preview.component';
import { AuthGuard } from '../auth/auth.guard';
import { HomePageImageHeaderViewComponent } from './home-page-image-header/home-page-image-header-view/home-page-image-header-view.component';
import { HomePageImageHeaderDetailComponent } from './home-page-image-header/home-page-image-header-detail/home-page-image-header-detail.component';
import { HomePageImageHeaderCreateComponent } from './home-page-image-header/home-page-image-header-create/home-page-image-header-create.component';
import { HomePageImageBarViewComponent } from './home-page-image-bar/home-page-image-bar-view/home-page-image-bar-view.component';
import { HomePageImageBarDetailComponent } from './home-page-image-bar/home-page-image-bar-detail/home-page-image-bar-detail.component';
import { HomePageImageBarCreateComponent } from './home-page-image-bar/home-page-image-bar-create/home-page-image-bar-create.component';
import { HomePageNavbarViewComponent } from './home-page-navbar/home-page-navbar-view/home-page-navbar-view.component';
import { HomePageNavbarDetailComponent } from './home-page-navbar/home-page-navbar-detail/home-page-navbar-detail.component';
import { HomePageNavbarCreateComponent } from './home-page-navbar/home-page-navbar-create/home-page-navbar-create.component';
import { HomePageAboutUsViewComponent } from './home-page-about-us/home-page-about-us-view/home-page-about-us-view.component';
import { HomePageAboutUsDetailComponent } from './home-page-about-us/home-page-about-us-detail/home-page-about-us-detail.component';
import { HomePageAboutUsCreateComponent } from './home-page-about-us/home-page-about-us-create/home-page-about-us-create.component';

const Routes: Routes = [
  { path: "homepage", component: HomePageViewComponent, canActivate:[AuthGuard]},
  { path: "homepage/navbar", component: HomePageNavbarViewComponent, canActivate:[AuthGuard] },
  { path: "homepage/navbar/:nav/edit", component: HomePageNavbarDetailComponent, canActivate:[AuthGuard] },
  { path: "homepage/navbar/create", component: HomePageNavbarCreateComponent, canActivate:[AuthGuard] },
  { path: "homepage/image-header", component: HomePageImageHeaderViewComponent, canActivate:[AuthGuard] },
  { path: "homepage/image-header/:id/edit", component: HomePageImageHeaderDetailComponent, canActivate:[AuthGuard] },
  { path: "homepage/image-header/create", component: HomePageImageHeaderCreateComponent, canActivate:[AuthGuard] },
  { path: "homepage/image-bar", component: HomePageImageBarViewComponent, canActivate:[AuthGuard] },
  { path: "homepage/image-bar/:img/edit", component: HomePageImageBarDetailComponent, canActivate:[AuthGuard] },
  { path: "homepage/image-bar/create", component: HomePageImageBarCreateComponent, canActivate:[AuthGuard] },
  { path: "homepage/preview", component: HomePagePreviewComponent, canActivate:[AuthGuard] },
  { path: "homepage/about-us", component: HomePageAboutUsViewComponent, canActivate:[AuthGuard] },
  { path: "homepage/about-us/edit", component: HomePageAboutUsDetailComponent, canActivate:[AuthGuard] },
  { path: "homepage/about-us/create", component: HomePageAboutUsCreateComponent, canActivate:[AuthGuard] },

];

@NgModule({
  declarations: [
    HomePagePreviewComponent, 
    HomePageImageHeaderViewComponent, 
    HomePageImageHeaderDetailComponent, 
    HomePageImageHeaderCreateComponent, 
    HomePageImageBarViewComponent, 
    HomePageImageBarDetailComponent, 
    HomePageImageBarCreateComponent, 
    HomePageNavbarViewComponent, 
    HomePageNavbarDetailComponent, 
    HomePageNavbarCreateComponent, 
    HomePageAboutUsViewComponent, 
    HomePageAboutUsDetailComponent, 
    HomePageAboutUsCreateComponent],
  imports: [
    CommonModule,
    FormsModule,
    [RouterModule.forRoot(Routes)],
  ],
  exports: [RouterModule]
})
export class HomepageModule { }
