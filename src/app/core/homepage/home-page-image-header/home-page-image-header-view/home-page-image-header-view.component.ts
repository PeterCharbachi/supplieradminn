import { Component, OnInit } from '@angular/core';
import { HomePageService } from 'src/app/shared/services/home-page/home-page.service';
import { IHeaderImage } from '../../homepage';

@Component({
  selector: 'app-home-page-image-header-view',
  templateUrl: './home-page-image-header-view.component.html',
  styleUrls: ['./home-page-image-header-view.component.css']
})
export class HomePageImageHeaderViewComponent implements OnInit {
  imgHeader: IHeaderImage = {} as IHeaderImage;
  constructor(private homepageApi: HomePageService,) { }

  ngOnInit() {
  let sid = JSON.parse(sessionStorage.getItem('secret'));
  sid = sid.user
  this.homepageApi.getImageHeaderBySupplier(sid).subscribe(
    data => {
      this.imgHeader = data;
  })
  
  }
  
}
