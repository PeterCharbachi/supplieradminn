import { Component, OnInit } from '@angular/core';
import { IHeaderImage } from '../../homepage';
import { HomePageService } from 'src/app/shared/services/home-page/home-page.service';

@Component({
  selector: 'app-home-page-image-header-detail',
  templateUrl: './home-page-image-header-detail.component.html',
  styleUrls: ['./home-page-image-header-detail.component.css']
})
export class HomePageImageHeaderDetailComponent implements OnInit {

  homePage: IHeaderImage = {} as IHeaderImage;
  selectedFile: File;
  constructor(private homepageApi: HomePageService) { }

  ngOnInit() {
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
 }
  onSave(){

    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user

    let formData = new FormData();
    formData.append('file', this.selectedFile);
    formData.append('homePageSupplier', this.homePage.homePageIB_sid = sid);

    this.homepageApi.putApiImageHeader(formData).subscribe(
      (data => {
        console.log(data);
        location.reload();
      })
    )
  }

}
