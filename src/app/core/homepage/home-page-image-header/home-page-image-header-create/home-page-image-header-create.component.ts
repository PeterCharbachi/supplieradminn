import { Component, OnInit } from '@angular/core';
import { HomePageService } from 'src/app/shared/services/home-page/home-page.service';
import { IHeaderImage } from '../../homepage';

@Component({
  selector: 'app-home-page-image-header-create',
  templateUrl: './home-page-image-header-create.component.html',
  styleUrls: ['./home-page-image-header-create.component.css']
})
export class HomePageImageHeaderCreateComponent implements OnInit {
  homePage: IHeaderImage = {} as IHeaderImage;
  selectedFile: File;
  constructor(private homepageApi: HomePageService) { }

  ngOnInit() {
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
 }
  onSave(){

    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user

    let formData = new FormData();
    formData.append('file', this.selectedFile);
    formData.append('homePageSupplier', this.homePage.homePageIB_sid = sid);

    this.homepageApi.postApiImageHeader(formData).subscribe(
      (data => {
        console.log(data);
        location.reload();
      })
    )
  }


}
