import { Component, OnInit } from '@angular/core';
import { IHomePageAboutUs } from '../../homepage';
import { HomePageService } from 'src/app/shared/services/home-page/home-page.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page-about-us-detail',
  templateUrl: './home-page-about-us-detail.component.html',
  styleUrls: ['./home-page-about-us-detail.component.css']
})
export class HomePageAboutUsDetailComponent implements OnInit {

  aboutUs: IHomePageAboutUs = {} as IHomePageAboutUs;
  constructor(private homepageApi: HomePageService, private _router: Router) { }

  
  ngOnInit() {
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user

    this.homepageApi.getApiAboutUs(sid).subscribe(
      data => {
        this.aboutUs = data;
    })
  }

  onSave(){

    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    this.aboutUs.supplier_id = sid;

    this.homepageApi.putApiAboutUs(this.aboutUs).subscribe(
      (data => {
        console.log(data);
        this._router.navigateByUrl('/homepage/image-bar');
      })
    )
  }

}
