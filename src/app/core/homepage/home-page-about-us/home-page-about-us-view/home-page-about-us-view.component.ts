import { Component, OnInit } from '@angular/core';
import { IHomePageAboutUs } from '../../homepage';
import { HomePageService } from 'src/app/shared/services/home-page/home-page.service';

@Component({
  selector: 'app-home-page-about-us-view',
  templateUrl: './home-page-about-us-view.component.html',
  styleUrls: ['./home-page-about-us-view.component.css']
})
export class HomePageAboutUsViewComponent implements OnInit {

  aboutUs: IHomePageAboutUs = {} as IHomePageAboutUs;
  constructor(private homepageApi: HomePageService) { }

  ngOnInit() {
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user

    this.homepageApi.getApiAboutUs(sid).subscribe(
      data => {
        this.aboutUs = data;
    })
  }

}
