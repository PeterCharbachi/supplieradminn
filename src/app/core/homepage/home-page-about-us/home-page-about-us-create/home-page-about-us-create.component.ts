import { Component, OnInit } from '@angular/core';
import { IHomePageAboutUs } from '../../homepage';
import { HomePageService } from 'src/app/shared/services/home-page/home-page.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page-about-us-create',
  templateUrl: './home-page-about-us-create.component.html',
  styleUrls: ['./home-page-about-us-create.component.css']
})
export class HomePageAboutUsCreateComponent implements OnInit {
  
  aboutUs: IHomePageAboutUs = {} as IHomePageAboutUs;

  constructor(private homepageApi: HomePageService,  private _router: Router) { }

  ngOnInit() {
  }
  onSave(){

    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    this.aboutUs.supplier_id = sid;

    this.homepageApi.postApiAboutUs(this.aboutUs).subscribe(
      (data => {
        console.log(data);
        this._router.navigateByUrl('/homepage/image-bar');
      })
    )
  }
}
