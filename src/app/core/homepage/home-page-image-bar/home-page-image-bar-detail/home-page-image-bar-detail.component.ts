import { Component, OnInit } from '@angular/core';
import { IImageBar } from '../../homepage';
import { HomePageService } from 'src/app/shared/services/home-page/home-page.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home-page-image-bar-detail',
  templateUrl: './home-page-image-bar-detail.component.html',
  styleUrls: ['./home-page-image-bar-detail.component.css']
})
export class HomePageImageBarDetailComponent implements OnInit {

  imgBar: IImageBar = {} as IImageBar;
  oneImgBar: IImageBar[] = [];
  selectedFile: File;
  constructor(private root: ActivatedRoute, private homepageApi: HomePageService, private _router: Router) { }
  
  ngOnInit() {
    const img = this.root.snapshot.paramMap.get('img');
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    this.homepageApi.getImageBarBySupplierAndImageId(img, sid).subscribe(
      data => {
        this.imgBar = data;
    })

  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }


  onDelete(){
    const img = this.root.snapshot.paramMap.get('img');
  
    this.homepageApi.deleteImagebarById(img).subscribe(
      (data => {
        console.log(data);
        this._router.navigateByUrl('/homepage/image-bar');
      })
    )
  
  }

  onSave(){

    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    const img = this.root.snapshot.paramMap.get('img');
    const name:any = this.imgBar.homepageIB_name;
    let formData = new FormData();
    formData.append('file', this.selectedFile);
    formData.append('img', this.imgBar.homepageIB_id = img);
    formData.append('name', name);

    this.homepageApi.putApiImagebar(formData).subscribe(
      (data => {
        console.log(data);
        this._router.navigateByUrl('/homepage/image-bar');
      })
    )
  }
}
