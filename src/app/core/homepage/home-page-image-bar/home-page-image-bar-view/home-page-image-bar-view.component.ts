import { Component, OnInit } from '@angular/core';
import { IImageBar } from '../../homepage';
import { HomePageService } from 'src/app/shared/services/home-page/home-page.service';

@Component({
  selector: 'app-home-page-image-bar-view',
  templateUrl: './home-page-image-bar-view.component.html',
  styleUrls: ['./home-page-image-bar-view.component.css']
})
export class HomePageImageBarViewComponent implements OnInit {
  imgBar: IImageBar[] = [];
  constructor(private homepageApi: HomePageService,) { }

  ngOnInit() {
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    this.homepageApi.getImageBarBySupplier(sid).subscribe(
      data => {
        this.imgBar = data;
    })
  }
  

}
