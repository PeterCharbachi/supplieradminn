import { Component, OnInit } from '@angular/core';
import { HomePageService } from 'src/app/shared/services/home-page/home-page.service';
import { IImageBar } from '../../homepage';
import { IProductView } from 'src/app/core/product/products';
import { ProductsService } from 'src/app/shared/services/products/products.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page-image-bar-create',
  templateUrl: './home-page-image-bar-create.component.html',
  styleUrls: ['./home-page-image-bar-create.component.css']
})
export class HomePageImageBarCreateComponent implements OnInit {
  products: IProductView[];
  imgBar: IImageBar = {} as IImageBar;
  selectedFile: File;
  selectedProduct:string = '0';
  constructor(private homepageApi: HomePageService, private suppliersApi: ProductsService, private _router: Router) { }
  
  ngOnInit() {

    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
  
    this.suppliersApi.getProductsBySuppliersId(sid).subscribe(
      data => {
        this.products = data;
    })
  }

  onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }

  onSave(){

    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    const name:any = this.imgBar.homepageIB_name;
    let formData = new FormData();
    formData.append('file', this.selectedFile);
    formData.append('supplierId', this.imgBar.supplierId = sid);
    formData.append('name', name);
    formData.append('product_id', this.selectedProduct);

    this.homepageApi.postApiImagebar(formData).subscribe(
      (data => {
        console.log(data);
        this._router.navigateByUrl('/homepage/image-bar');
      })
    )
  }

}
