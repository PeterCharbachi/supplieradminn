export interface INavbar{
    homePageN_name: string;
    homePageN_sid: number;
    homePageN_category: number;
    homePageN_id: string;
    
}

export interface IHeaderImage{
    homePageIB_id: number;
    homePageIB_sid: number;
    homePageIB_Image: string;
}

export interface IImageBar{
    supplierId: number;
    homepageIB_name: number;
    homepageIB_id: string;

}
export interface IHomePageAboutUs{
    supplier_id: number;
    address: number;
    zipCode: number;
    city: string;
    number: number;
    email: string;
    textField: string;
}