import { Component, OnInit } from '@angular/core';
import { HomePageService } from 'src/app/shared/services/home-page/home-page.service';
import { INavbar, IHeaderImage, IImageBar } from '../homepage';

@Component({
  selector: 'app-home-page-preview',
  templateUrl: './home-page-preview.component.html',
  styleUrls: ['./home-page-preview.component.css']
})
export class HomePagePreviewComponent implements OnInit {
  navbar: INavbar[] = []
  imgHeader: IHeaderImage = {} as IHeaderImage;
  imgBar: IImageBar[] = [];
  constructor(private homepageApi: HomePageService,) { }

  ngOnInit() {
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user

    this.homepageApi.getNavbarBySupplier(sid).subscribe(
      data => {
        this.navbar = data;
    })
    this.homepageApi.getImageHeaderBySupplier(sid).subscribe(
      data => {
        this.imgHeader = data;
    })
    this.homepageApi.getImageBarBySupplier(sid).subscribe(
      data => {
        this.imgBar = data;
    })
  }

}
