import { Component, OnInit } from '@angular/core';
import { HomePageService } from 'src/app/shared/services/home-page/home-page.service';
import { CategoryService } from 'src/app/shared/services/category/category.service';
import { Router, ActivatedRoute } from '@angular/router';
import { INavbar } from '../../homepage';
import { ISuppliersCategoryView } from 'src/app/core/category/category';

@Component({
  selector: 'app-home-page-navbar-detail',
  templateUrl: './home-page-navbar-detail.component.html',
  styleUrls: ['./home-page-navbar-detail.component.css']
})
export class HomePageNavbarDetailComponent implements OnInit {

  navbar: INavbar = {} as INavbar;
  categories: ISuppliersCategoryView[] = []
  addNavbar: INavbar = {} as INavbar;
  test: number = 0;
  selectedOption: number;
  printedOption: number;
  selectedCategory:any = '0';

  constructor(private root: ActivatedRoute, private homepageApi: HomePageService, private categoryApi: CategoryService, private _router: Router) { }

  ngOnInit() {
    // this.addNavbar.category_id = this.test;
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    const nav = this.root.snapshot.paramMap.get('nav');
    this.homepageApi.getNavbarBySupplierAndNavId(sid, nav).subscribe(
      data => {
        this.navbar = data;
        let categoryId:string =  this.navbar.homePageN_category.toString();
        this.selectedCategory = categoryId;
    })
  
    this.categoryApi.getSuppliersCategories(sid).subscribe(
      data => {
        this.categories = data;

    })
  }

  onDelete(){
    const nav = this.root.snapshot.paramMap.get('nav');
  
    this.homepageApi.deleteNavbarById(nav).subscribe(
      (data => {
        console.log(data);
        this._router.navigateByUrl('/homepage/navbar');
      })
    )
  
  }

  updateNavbar() {
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    const nav = this.root.snapshot.paramMap.get('nav');
    this.printedOption = this.selectedOption;
    this.addNavbar.homePageN_sid = sid;
    this.addNavbar.homePageN_category = this.selectedCategory;
    this.addNavbar.homePageN_id = nav;
    this.addNavbar.homePageN_name = this.navbar.homePageN_name;
    this.homepageApi.putApiNavbar(this.addNavbar).subscribe(
      (data => { 
        this._router.navigateByUrl('/homepage/navbar');
      })
    )
  }
}
