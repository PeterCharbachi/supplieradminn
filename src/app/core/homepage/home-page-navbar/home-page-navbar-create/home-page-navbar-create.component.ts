import { Component, OnInit } from '@angular/core';
import { HomePageService } from 'src/app/shared/services/home-page/home-page.service';
import { INavbar } from '../../homepage';
import { ISuppliersCategoryView } from 'src/app/core/category/category';
import { CategoryService } from 'src/app/shared/services/category/category.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page-navbar-create',
  templateUrl: './home-page-navbar-create.component.html',
  styleUrls: ['./home-page-navbar-create.component.css']
})
export class HomePageNavbarCreateComponent implements OnInit {
  navbar: INavbar[] = []
  categories: ISuppliersCategoryView[] = []
  addNavbar: INavbar = {} as INavbar;
  test: number = 0;
  selectedCategory:string = "0";
  printedOption: number;
  constructor(private homepageApi: HomePageService, private categoryApi: CategoryService, private _router: Router) { }

  ngOnInit() {
    // this.addNavbar.category_id = this.test;
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
  
    this.homepageApi.getNavbarBySupplier(sid).subscribe(
      data => {
        this.navbar = data;

    })
  
    this.categoryApi.getSuppliersCategories(sid).subscribe(
      data => {
        this.categories = data;
    })
  }


  saveNavbar() {
    
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    
    let cid:any = this.selectedCategory;
    this.addNavbar.homePageN_sid = sid;
    this.addNavbar.homePageN_category = cid
    this.homepageApi.postApiNavbar(this.addNavbar).subscribe(
      (data => { 
        this._router.navigateByUrl('/homepage/navbar');
      })
    )
  }
}
