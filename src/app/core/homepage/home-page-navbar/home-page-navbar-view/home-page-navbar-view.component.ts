import { Component, OnInit } from '@angular/core';
import { INavbar } from '../../homepage';
import { ISuppliersCategoryView } from 'src/app/core/category/category';
import { HomePageService } from 'src/app/shared/services/home-page/home-page.service';
import { CategoryService } from 'src/app/shared/services/category/category.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page-navbar-view',
  templateUrl: './home-page-navbar-view.component.html',
  styleUrls: ['./home-page-navbar-view.component.css']
})
export class HomePageNavbarViewComponent implements OnInit {

  navbar: INavbar[] = []
  categories: ISuppliersCategoryView[] = []
  addNavbar: INavbar = {} as INavbar;
  test: number = 0;
  selectedOption: number;
  printedOption: number;

  constructor(private homepageApi: HomePageService, private categoryApi: CategoryService, private _router: Router) { }

  ngOnInit() {
    // this.addNavbar.category_id = this.test;
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
  
    this.homepageApi.getNavbarBySupplier(sid).subscribe(
      data => {
        this.navbar = data;
    })
  
    this.categoryApi.getSuppliersCategories(sid).subscribe(
      data => {
        this.categories = data;
    })

    this.homepageApi.getNavbarBySupplier(sid).subscribe(
      data => {
        this.navbar = data;
    })
  }
}
