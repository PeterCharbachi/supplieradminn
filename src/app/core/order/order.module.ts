import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderViewComponent } from './order-view/order-view.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import { Routes, RouterModule } from '@angular/router';
import { MatInputModule, MatFormFieldModule, MatTableModule, MatPaginatorModule, MatSortModule, MatRippleModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuard } from '../auth/auth.guard';


const Routes: Routes = [
  { path: "orders", component: OrderViewComponent, canActivate:[AuthGuard] },
  { path: "orders/:oid", component: OrderDetailComponent, canActivate:[AuthGuard] },
];

@NgModule({
  declarations: [
    OrderViewComponent, 
    OrderDetailComponent, 
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    ReactiveFormsModule, 
    
    [RouterModule.forRoot(Routes)],
  ],
  exports: [RouterModule]
})
export class OrderModule { }
