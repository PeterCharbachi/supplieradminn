import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { IOrderView } from '../order';
import { OrderService } from 'src/app/shared/services/order/order.service';

@Component({
  selector: 'app-order-view',
  templateUrl: './order-view.component.html',
  styleUrls: ['./order-view.component.css']
})
export class OrderViewComponent implements OnInit {
   order: IOrderView[] = [];
  displayedColumns = ['id', 'name', 'email', 'leveransadress'];
  dataSource: MatTableDataSource<IOrderView>;
 
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private suppliersApi: OrderService) {
     this.dataSource = new MatTableDataSource();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
  
    this.suppliersApi.getOrderBySuppliersId(sid).subscribe(
      data => {
        this.dataSource.data = data;
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
