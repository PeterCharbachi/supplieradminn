import { Component, OnInit } from '@angular/core';
import { IOrderView } from '../order';
import { OrderService } from 'src/app/shared/services/order/order.service';
import { ActivatedRoute } from '@angular/router';
import { IProductView } from '../../product/products';
import { ProductsService } from 'src/app/shared/services/products/products.service';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit {
  products: IProductView[] = [];
  order: IOrderView = {} as IOrderView;


  constructor(private root: ActivatedRoute, private suppliersApi: OrderService, private productApi: ProductsService) { }

  ngOnInit() {

    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    const oid = this.root.snapshot.paramMap.get('oid');
    

    this.productApi.getProductsBySuppliersId(sid).subscribe(
      data => {
        this.products = data;
    })

    this.suppliersApi.getProductsByOrderId(sid, oid).subscribe(
      data => {
        this.order = data;
        this.order.pid = this.products[0].product_id;
        this.order.price = this.products[0].price;
    })
  }

}
