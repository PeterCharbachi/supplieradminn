export interface IOrderView extends IOrderDetail{
    oid: number;
    first_name: string;
    last_name: string;
    email: string;
}

export interface IOrderDetail{
    oid: number;
    pid: number;
    amount: number;
    price: number;
    product_name: string;
}

export interface ISubCategoryView{
    subcategory_id: number;
    sub_name: string;
    supplier_id: number;
    category_id: number;
}