import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { ISuppliersCategoryView } from '../category';
import { CategoryService } from 'src/app/shared/services/category/category.service';

@Component({
  selector: 'app-category-view',
  templateUrl: './category-view.component.html',
  styleUrls: ['./category-view.component.css']
})
export class CategoryViewComponent implements OnInit {

  products: ISuppliersCategoryView[] = [];
  displayedColumns = ['id', 'name', 'description', 'category_id'];
  dataSource: MatTableDataSource<ISuppliersCategoryView>;
 
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;


  constructor(private suppliersApi: CategoryService) {
     this.dataSource = new MatTableDataSource();
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
  
    this.suppliersApi.getSuppliersCategories(sid).subscribe(
      data => {
        this.dataSource.data = data;
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
