import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryViewComponent } from './category-view/category-view.component';
import { CategoryCreateComponent } from './category-create/category-create.component';
import { CategoryDetailComponent } from './category-detail/category-detail.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';

const Routes: Routes = [
  { path: "category", component: CategoryViewComponent, canActivate:[AuthGuard] },
  { path: "category/create", component: CategoryCreateComponent, canActivate:[AuthGuard] },
  { path: "category/:cid/edit", component: CategoryDetailComponent, canActivate:[AuthGuard] },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    [RouterModule.forRoot(Routes)],
  ],
  exports: [RouterModule]
})
export class CategoryModule { }
