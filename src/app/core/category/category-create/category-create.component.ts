import { Component, OnInit, } from '@angular/core';
import { ICategoryView, ISubCategoryView } from '../category';
import { CategoryService } from 'src/app/shared/services/category/category.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-category-create',
  templateUrl: './category-create.component.html',
  styleUrls: ['./category-create.component.css']
})
export class CategoryCreateComponent implements OnInit {
  categorylist = [{name:"Kategori", id:'1'},{name:"Under kategori", id:'2'}];
  category: ICategoryView = {} as ICategoryView
  subCategory: ISubCategoryView = {} as ISubCategoryView
  selectedCategory:string = '0';

  constructor(private suppliersApi: CategoryService, private _router: Router) { }

  ngOnInit() {
  }
  saveCategory(){
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    this.category.supplier_id = sid;
    this.suppliersApi.postApiCategory(this.category).subscribe(
      (data => { 
        this._router.navigateByUrl('/category');
      })
    )
    
  }
  saveSubCategory(){
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    this.subCategory.supplier_id = sid;
    this.suppliersApi.postApiSubCategory(this.subCategory).subscribe(
      (data => { 
        this._router.navigateByUrl('/category');
      })
    )
    
  }
  
  

}
