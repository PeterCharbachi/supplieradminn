import { Component, OnInit,} from '@angular/core';
import { CategoryService } from 'src/app/shared/services/category/category.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ICategoryView, ISubCategoryView, ISuppliersCategoryUpdate } from '../category';


@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.css']
})
export class CategoryDetailComponent implements OnInit {
  category: ICategoryView = {} as ICategoryView;
  updateCategories: ISuppliersCategoryUpdate = {} as ISuppliersCategoryUpdate;
  subCategory: ISubCategoryView[] = [];
  subCategoryView: ISubCategoryView[] = [];
  subCategoryObject: ISubCategoryView = {} as ISubCategoryView;
  subish: ISubCategoryView[] = [];

  constructor(private root: ActivatedRoute, private suppliersApi: CategoryService, private _router: Router,) {

  }

  ngOnInit() {
    let sid = JSON.parse(sessionStorage.getItem('secret'));
    sid = sid.user
    const cid = this.root.snapshot.paramMap.get('cid');
    this.suppliersApi.getCategoryById(sid, cid).subscribe(
      data => {
        this.updateCategories = data;
    })

    this.suppliersApi.getSubCategories(sid).subscribe(
      data => {
        this.subCategory = data;
    })

    this.suppliersApi.getSubCategoryByCategoryId(cid).subscribe(
      data => {
        this.subCategoryView = data;
    })
    
    
  }

 // getSubCategoryByCategoryId

  updateCategory() {

    this.suppliersApi.putApiCategory(this.updateCategories).subscribe(
      data => {
        this.updateCategories = data;
        this._router.navigateByUrl('/category');
      }
    )
  }

  
  updateSubCategory() {
    const cid = this.root.snapshot.paramMap.get('cid');
    this.suppliersApi.putApiSubCategory(cid, this.subCategoryObject).subscribe(
      data => {
        this.subCategoryObject = data;
        
      }
    )
  }

   
  onDelete(){
    const cid = this.root.snapshot.paramMap.get('cid');
  
    this.suppliersApi.deleteCategoriesById(cid).subscribe(
      (data => {
        console.log(data);
        this._router.navigateByUrl('/category');
      })
    )
  
  }

  deleteSub(){
    const cid = this.subCategoryView[0];
    this.suppliersApi.deleteSubCategoriesById(cid).subscribe(
      (data => {
        console.log(data);
        this._router.navigateByUrl('/category');
      })
    )
  
  }

 
}
