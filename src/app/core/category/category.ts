export interface ISuppliersCategoryUpdate{
    category_id: number;
    name: string;
    description: boolean;
    supplier_id: number;
}

export interface ISuppliersCategoryView{
    supplier_category_id: number;
    name: string;
    description: boolean;
    supplier_id: number;
}

export interface ICategoryView{
    category_id: number;
    category_name: string;
    description: boolean;
    supplier_id: number;
}

export interface ISubCategoryView{
    subcategory_id: number;
    sub_name: string;
    supplier_id: number;
    category_id: number;
}